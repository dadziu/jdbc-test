package pl.dadziu;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

//DataSource
public class MainDataSource {

    private static final String DATABASE_NAME = "bd_zsi2018";
    private static final String SERVER_NAME = "localhost";
    private static final String USER = "root";
    private static final String PASSWORD = "";

    public static void main(String[] args) {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setDatabaseName(DATABASE_NAME);
        dataSource.setServerName(SERVER_NAME); //same as hostname
        dataSource.setUser(USER);
        dataSource.setPassword(PASSWORD);

        try {
            Connection connection = dataSource.getConnection(); //(DATABASE_NAME, SERVER_NAME, USER, PASSWORD)
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from pojazdy where POJEMNOSC > 0");

            while(resultSet.next()) {
                String marka = resultSet.getString("MARKA");
                String kolor = resultSet.getString(6);
                double pojemnosc = resultSet.getDouble("POJEMNOSC");
                System.out.println("marka: " + marka + "|| kolor: " + kolor + "|| pojemnosc: " + pojemnosc);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
