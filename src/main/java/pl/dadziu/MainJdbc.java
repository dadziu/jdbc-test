package pl.dadziu;

import java.sql.*;

//DriverManager
public class MainJdbc {

    private static final String USER = "root";
    private static final String PASSWORD = "";
    private static final String URL = "jdbc:mysql://localhost:3306/bd_zsi2018";

    public static void main(String[] args) {
        //opisuje polaczenie do bazy danych:
        try {
            Connection connection = DriverManager.getConnection(URL, USER, PASSWORD);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from pojazdy where POJEMNOSC > 0");

            while(resultSet.next()) {
                String marka = resultSet.getString("MARKA");
                String kolor = resultSet.getString(6);
                double pojemnosc = resultSet.getDouble("POJEMNOSC");
                System.out.println("marka: " + marka + "|| kolor: " + kolor + "|| pojemnosc: " + pojemnosc);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

